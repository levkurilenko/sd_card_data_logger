// Author: Lev Kurilenko
// Date: 3/10/18
// Project: Rocket Sensor and Data Logger
//
// Some code leveraged from:
// https://cdn-learn.adafruit.com/downloads/pdf/dht.pdf
// https://howtomechatronics.com/tutorials/arduino/arduino-sd-card-data-logging-excel-tutorial/

#include <SD.h>
#include <SPI.h>
#include "DHT.h"

#define DHTPIN 2     // what digital pin we're connected to
#define DHTTYPE DHT11   // DHT 11

// Connect pin 1 (on the left) of the sensor to +5V
// NOTE: If using a board with 3.3V logic like an Arduino Due connect pin 1
// to 3.3V instead of 5V!
// Connect pin 2 of the sensor to whatever your DHTPIN is
// Connect pin 4 (on the right) of the sensor to GROUND
// Connect a 10K resistor from pin 2 (data) to pin 1 (power) of the sensor

// Initialize DHT sensor.
// Note that older versions of this library took an optional third parameter to
// tweak the timings for faster processors.  This parameter is no longer needed
// as the current DHT reading algorithm adjusts itself to work on faster procs.
DHT dht(DHTPIN, DHTTYPE);

File myFile;

int pinCS = 10; // Pin 10 on Arduino Uno

void setup() {
    
  Serial.begin(9600);
  Serial.println("DHT11 test!");
  pinMode(pinCS, OUTPUT);
  dht.begin();
  
  // SD Card Initialization
  if (SD.begin()) {
    Serial.println("SD card is ready to use.");
  }
  else {
    Serial.println("SD card initialization failed");
    return;
  }

  // Remove the file if it already exists
  SD.remove("temp_log.csv");
  Serial.println("Existing temp_log.csv file removed!");

  // Open file
  myFile = SD.open("temp_log.csv", FILE_WRITE);
  
  // If the file opened okay, write to it:
  if (myFile) {
    Serial.println("Writing to file...");
    // Write to file
    myFile.println("Time (s), Humidity (%), Temperature (C), Temperature (F), Heat Index (C), Heat Index (F)");
    myFile.close(); // close the file
    Serial.println("Done.");
  }
  // If the file didn't open, print an error:
  else {
    Serial.println("error opening temp_log.csv");
  }
}


void loop() {
  // Wait a few seconds between measurements.
  delay(2000);

  // Reading temperature or humidity takes about 250 milliseconds!
  // Sensor readings may also be up to 2 seconds 'old' (its a very slow sensor)
  float h = dht.readHumidity();
  // Read temperature as Celsius (the default)
  float t = dht.readTemperature();
  // Read temperature as Fahrenheit (isFahrenheit = true)
  float f = dht.readTemperature(true);

  // Compute heat index in Fahrenheit (the default)
  float hif = dht.computeHeatIndex(f, h);
  // Compute heat index in Celsius (isFahreheit = false)
  float hic = dht.computeHeatIndex(t, h, false);
  
  // Create/Open file 
  myFile = SD.open("temp_log.csv", FILE_WRITE);
  
  // if the file opened okay, write to it:
  if (myFile) {
    Serial.println("Writing to file...");
    // Write logged data to file
    myFile.print(((float) micros())/1000000);
    myFile.print(",");
    myFile.print(h);
    myFile.print(",");
    myFile.print(t);
    myFile.print(",");
    myFile.print(f);
    myFile.print(",");
    myFile.print(hic);
    myFile.print(",");
    myFile.println(hif);
    myFile.close(); // close the file
    Serial.println("Done.");
  }
  // If the file didn't open, print an error:
  else {
    Serial.println("error opening temp_log.csv");
  }
}
